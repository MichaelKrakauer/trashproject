﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using TrashProject.Presenters.Interfaces;
using TrashProject.Utils;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.UI.Xaml;
using System.Threading.Tasks;

namespace TrashProject.Presenters
{
    public class TrashPresenter : INotifyPropertyChanged, IMediaCaptureUser
    {

        public MediaCapture CameraCapture { get; set; }
        public ICommand SnapshotCommand { get; set; }
        public ICommand ConfigurationCommand { get; set; }

        private string _imagePath;
        private bool _takingPhoto;
        private bool _isSetToMaxResolution = true;

        public string ImagePath
        {
            get { return _imagePath; }
            private set
            {
                _imagePath = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public TrashPresenter()
        {
            CameraCapture = new MediaCapture();
            SnapshotCommand = new DelegateCommand(Snapshot);
            ConfigurationCommand = new DelegateCommand(Configuration);
        }

        private void Configuration()
        {
            //var streamTypes = Enum.GetValues(typeof(MediaStreamType)).OfType<MediaStreamType>();

            //foreach (var item in streamTypes.Where(x => x != MediaStreamType.Audio))
            //{
            //    var typeSettings = CameraCapture.VideoDeviceController.GetAvailableMediaStreamProperties(item).Where(x => x is VideoEncodingProperties);
            //    typeSettings = typeSettings.Select(x => x as VideoEncodingProperties).OrderBy(x => x.Width * x.Height);
            //    var videoSetting = typeSettings.LastOrDefault() as VideoEncodingProperties;
            //    var sizes = typeSettings.Select(x => x as VideoEncodingProperties);


            //    if (videoSetting != null)
            //    {
            //        Debug.WriteLine(item);
            //        Debug.WriteLine("Max size:");
            //        Debug.WriteLine(videoSetting.Width * videoSetting.Height);
            //        Debug.WriteLine("All sizes:");
            //        foreach (var thing in sizes)
            //        {
            //            Debug.WriteLine(thing.Width * thing.Height / (1024d * 1024d));
            //        }
            //        Debug.WriteLine(string.Empty);
            //    }
            //}

            VideoEncodingProperties settings;
            var availabeSettings = CameraCapture.VideoDeviceController.GetAvailableMediaStreamProperties(MediaStreamType.Photo).Where(x => x is VideoEncodingProperties);
            availabeSettings = availabeSettings.Select(x => x as VideoEncodingProperties).OrderBy(x => x.Width * x.Height);
            if (!_isSetToMaxResolution)
            {
                settings = availabeSettings.Last() as VideoEncodingProperties;
            }
            else
            {
                settings = availabeSettings.First() as VideoEncodingProperties;
            }

            _isSetToMaxResolution = !_isSetToMaxResolution;
            Task.Run(() => CameraCapture.VideoDeviceController.SetMediaStreamPropertiesAsync(MediaStreamType.Photo, settings));
        }

        private async void Snapshot()
        {
            if (!_takingPhoto)
            {
                _takingPhoto = true;

                //var s = await CameraCapture.PrepareVariablePhotoSequenceCaptureAsync(ImageEncodingProperties.CreateJpeg());                
                var rootFolder = KnownFolders.CameraRoll;//ApplicationData.Current.LocalFolder;
                var file = await rootFolder.CreateFileAsync("THIS_IS_A_TEST.jpg", CreationCollisionOption.GenerateUniqueName);
                var t = DateTime.Now;
                await CameraCapture.CapturePhotoToStorageFileAsync(ImageEncodingProperties.CreateJpeg(), file);
                Debug.WriteLine(DateTime.Now - t);
                ImagePath = file.Path;
                var props = await file.GetBasicPropertiesAsync();
                Debug.WriteLine((double)props.Size / (1024d * 1024d));
                var properties = CameraCapture.VideoDeviceController.GetMediaStreamProperties(MediaStreamType.Photo) as VideoEncodingProperties;
                Debug.WriteLine("{0} * {1}", properties.Width, properties.Height);
                _takingPhoto = false;
            }
        }

        public void OnMediaCaptureInitialized()
        {

        }
    }
}
