﻿using Microsoft.Xaml.Interactivity;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TrashProject.Presenters.Interfaces;
using Windows.Devices.Enumeration;
using Windows.Media.Capture;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.Media.Devices;
using System.Diagnostics;

namespace TrashProject.Behaviors
{
    public class CaptureElementInitializationBehavior : DependencyObject, IBehavior
    {

        public MediaCapture CaptureSource
        {
            get { return (MediaCapture)GetValue(CaptureSourceProperty); }
            set { SetValue(CaptureSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CaptureSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CaptureSourceProperty =
            DependencyProperty.Register("CaptureSource", typeof(MediaCapture), typeof(CaptureElementInitializationBehavior), new PropertyMetadata(null, OnCaptureSourceAcquired));

        async private static void OnCaptureSourceAcquired(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                await (d as CaptureElementInitializationBehavior).InitializeCaptureSource(e.NewValue as MediaCapture);
            }
        }

        private async Task InitializeCaptureSource(MediaCapture newValue)
        {
            //var settings = new MediaCaptureInitializationSettings()
            //{
            //    StreamingCaptureMode = StreamingCaptureMode.Video,
            //    PhotoCaptureSource = PhotoCaptureSource.Photo,
            //    VideoDeviceId = (await DeviceInformation.FindAllAsync(DeviceClass.VideoCapture)).ToList()[0].Id
            //};

            //await CaptureSource.InitializeAsync(settings);
            await CaptureSource.InitializeAsync();
            Debug.WriteLine(CaptureSource.VideoDeviceController.PrimaryUse);
            (AssociatedObject as CaptureElement).Source = newValue;
            await CaptureSource.StartPreviewAsync();

            CaptureSource.SetPreviewRotation(VideoRotation.Clockwise90Degrees);
            await CaptureSource.ClearEffectsAsync(MediaStreamType.Photo);
            ((AssociatedObject as CaptureElement).DataContext as IMediaCaptureUser).OnMediaCaptureInitialized();
        }

        public DependencyObject AssociatedObject
        {
            get;
            set;
        }

        public void Attach(DependencyObject associatedObject)
        {
            AssociatedObject = associatedObject;
        }

        public void Detach()
        {
        }
    }
}
