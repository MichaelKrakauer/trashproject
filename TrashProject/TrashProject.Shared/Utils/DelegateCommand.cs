﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace TrashProject.Utils
{
    public class DelegateCommand : ICommand
    {
        private Action _action;

        public event EventHandler CanExecuteChanged;

        public DelegateCommand(Action action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action();
        }
    }

    public class DelegateCommand<T> : ICommand
    {
        private Action<T> _action;

        public event EventHandler CanExecuteChanged;

        public DelegateCommand(Action<T> action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action((T)parameter);
        }
    }
}
